import threading
import sys
import socket

def client():

    #Pull command line arguments
    rs_hostname = sys.argv[1]
    rs_port = int(sys.argv[2])
    ts_port = int(sys.argv[3])
     
    try:
        rs = socket.socket(socket.AF_INET, socket.SOCK_STREAM) #cs = client socket
        print("[C]: Client socket to rs created")
    except socket.error as err:
        print('socket open error: {} \n'.format(err))
        exit()
    
    #Bind server and host and connect
    rs_address = socket.gethostbyname(rs_hostname) 
    server_binding = (rs_address, rs_port)
    rs.connect(server_binding)

    fr = open("PROJI-HNS.txt", "r")
    fw = open("RESOLVED.txt", "w")
    lines = fr.read().splitlines()

    tsConnected = False


    for name in lines:
        #Send and recieve data
        rs.send(name.encode('utf-8'))
        data_from_server = rs.recv(1024)
        response = data_from_server.decode('utf-8')
        words = response.split()

        #Check flag
        if(words[len(words)-1] == 'A'): #Root NS found ip of hostname
            fw.write(response)
        elif(words[len(words)-1] == 'NS'): #Root NS did not find ip of hostname. Query top-level NS

            #Connect to top level server if not already
            if(not tsConnected):

                try:
                    ts = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                    print("[C]: Client socket to ts created")
                except socket.error as err:
                    print('socket open error: {} \n'.format(err))
                    exit()


                ts_hostname = words[0]
                ts_address = socket.gethostbyname(ts_hostname) 
                server_binding = (ts_address, ts_port)
                ts.connect(server_binding)
                tsConnected = True

            #Send, recieve, and write data
            ts.send(name.encode('utf-8'))
            data_from_server = ts.recv(1024)
            response = data_from_server.decode('utf-8')
            fw.write(response)

    #Close files and socket connections
    fr.close() ; fw.close()
    rs.close() ; ts.close()

    "[C]: rs and ts connections closed"
    exit()



if __name__ == "__main__":
    t2 = threading.Thread(name='client', target=client)
    t2.start()

    print("Done.")
