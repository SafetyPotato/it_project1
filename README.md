0. Thomas Nardello, tan89; Jilvia D'Souza, jmd664

1. (Our implementation was iterative and piazza post said project description was supposed to say iterative instead of recursive) We implemented our iterative functionality by sending each of the hostnames listed in "PROJI-HNS.txt" to rs (rs.py) from client (client.py). Rs compares each entry of the dns table to the hostname sent by client. If there is a match, rs sends the entry back to the client. If there isn't a match it instead sends TSHostname - NS. Client parses the response and determines what to do depending on the flag. If the flag is 'A' it writes to "RESOLVED.txt", otherwise it sends the same hostname it sent to rs, to ts (ts.py) using the hostname that rs returned. Ts has almost the same code and functionality as rs with the only major difference being that it sends an error message if a match is not found. Finally client writes the response to "RESOLVED.txt". Both rs and ts stay active until the client closes the connection.

2. Not that we know of.

3. The main problem we faced was that our rs and ts where closing after only one query. We fixed this issue by creating an infinite loop that would continuously wait to recieve data from the client until the client ends the connection.

4. While we were already were familiar with DNS from the lectures and readings, implementing this simplified version of a DNS system gave a greater understanding of how IP's of hostnames are queried when searching the Internet. Essentially working on this project has made it much easier to understand how DNS works on a larger scale.
